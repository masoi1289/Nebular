import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: AppComponent},
    { path: 'ahihi', loadChildren: 'app/test1/test1.module#Test1Mozdule' }
  ];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
export class AppRoutingModule {}
