import { NgModule } from '@angular/core';

import { AhihiComponent } from './ahihi';

import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    { path: '', component: AhihiComponent},
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class Test1RoutingModule {}
