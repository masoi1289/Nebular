import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router'; // we also need angular router for Nebular to function properly
import { NbSidebarModule, NbLayoutModule, NbSidebarService } from '@nebular/theme';
import { AhihiComponent } from './ahihi/ahihi.component';
import { Test1RoutingModule } from './test1.routing';

@NgModule({
  imports: [
    Test1RoutingModule,
    CommonModule,
    RouterModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbLayoutModule,
    NbSidebarModule,
  ],
  declarations: [NbSidebarService, AhihiComponent]
})
export class Test1Module { }
