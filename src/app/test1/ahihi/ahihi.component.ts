import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ahihi',
  template: `

    <nb-layout>
      <nb-layout-header fixed>Company Name</nb-layout-header>

      <nb-sidebar>Sidebar Content</nb-sidebar>

      <nb-layout-column>Page Content</nb-layout-column>
    </nb-layout>
  `,
  styleUrls: ['./ahihi.component.css']
})
export class AhihiComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
